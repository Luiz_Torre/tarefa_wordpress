<?php
/**
 * As configurações básicas do WordPress
 *
 * O script de criação wp-config.php usa esse arquivo durante a instalação.
 * Você não precisa usar o site, você pode copiar este arquivo
 * para "wp-config.php" e preencher os valores.
 *
 * Este arquivo contém as seguintes configurações:
 *
 * * Configurações do MySQL
 * * Chaves secretas
 * * Prefixo do banco de dados
 * * ABSPATH
 *
 * @link https://wordpress.org/support/article/editing-wp-config-php/
 *
 * @package WordPress
 */

// ** Configurações do MySQL - Você pode pegar estas informações com o serviço de hospedagem ** //
/** O nome do banco de dados do WordPress */
define( 'DB_NAME', 'tarefawordpress' );

/** Usuário do banco de dados MySQL */
define( 'DB_USER', 'admin' );

/** Senha do banco de dados MySQL */
define( 'DB_PASSWORD', 'admin' );

/** Nome do host do MySQL */
define( 'DB_HOST', 'localhost' );

/** Charset do banco de dados a ser usado na criação das tabelas. */
define( 'DB_CHARSET', 'utf8mb4' );

/** O tipo de Collate do banco de dados. Não altere isso se tiver dúvidas. */
define( 'DB_COLLATE', '' );

/**#@+
 * Chaves únicas de autenticação e salts.
 *
 * Altere cada chave para um frase única!
 * Você pode gerá-las
 * usando o {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org
 * secret-key service}
 * Você pode alterá-las a qualquer momento para invalidar quaisquer
 * cookies existentes. Isto irá forçar todos os
 * usuários a fazerem login novamente.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         '.NY2^,?T&C3Wa>;QO$X( $dr*Ul7Oz>vU/MN82tgV6tRB1yO6}6Hk`]9{`tEp3Y*' );
define( 'SECURE_AUTH_KEY',  '5M[SNd=C3,e}iPJrqF+$g(RnSTA3}R<(M*UXdfxo1-IJ~R;Z1?8oz2VWm[QBzjQ]' );
define( 'LOGGED_IN_KEY',    'qSKZR>B&O7G_h2y9Q!lG?2B|^?6*URt[s<Zd*O>VC=]l~2c1wsFc5;KI-gV9Ijch' );
define( 'NONCE_KEY',        '=,RPxgsN;7~|C,`q[4X/$-b_5Xey$7K)kd52+8BP.w=krh(y-ET 7$!]J0mt|gv&' );
define( 'AUTH_SALT',        '~A:^hN4l]3jbt~,JLB$/wxT$!>_MhLnzt7<vv$T96<MbW`ioHW9x7&-+H6Sn2#(D' );
define( 'SECURE_AUTH_SALT', '>1!CwQ<]6BHGAMoC=z3z#[>{C,qL]gME56V7eWIoYL4IxQCF7|^ed8L6k;rPx*o`' );
define( 'LOGGED_IN_SALT',   'b <G*,bA4(/PoM+6[^smJc`6D]aEJXTv*vQkT1JyZ:|LNzI }u~qGxcCG{]xVZ0&' );
define( 'NONCE_SALT',       '0&B(BhA o<oYN4%L6Fe&u%mBViVYMDzVzt[=G9pt5Wto[VO5{d$tDr[~0u}i{l.>' );

/**#@-*/

/**
 * Prefixo da tabela do banco de dados do WordPress.
 *
 * Você pode ter várias instalações em um único banco de dados se você der
 * um prefixo único para cada um. Somente números, letras e sublinhados!
 */
$table_prefix = 'wp_';

/**
 * Para desenvolvedores: Modo de debug do WordPress.
 *
 * Altere isto para true para ativar a exibição de avisos
 * durante o desenvolvimento. É altamente recomendável que os
 * desenvolvedores de plugins e temas usem o WP_DEBUG
 * em seus ambientes de desenvolvimento.
 *
 * Para informações sobre outras constantes que podem ser utilizadas
 * para depuração, visite o Codex.
 *
 * @link https://wordpress.org/support/article/debugging-in-wordpress/
 */
define( 'WP_DEBUG', false );

/* Isto é tudo, pode parar de editar! :) */

/** Caminho absoluto para o diretório WordPress. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', __DIR__ . '/' );
}

/** Configura as variáveis e arquivos do WordPress. */
require_once ABSPATH . 'wp-settings.php';
