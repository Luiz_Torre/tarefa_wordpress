<?php

    remove_action('wp_head', 'rsd_link');
    remove_action('wp_head', 'wlwmanifest_link');
    remove_action('wp_head', 'start_post_rel_link', 10, 0 );
    remove_action('wp_head', 'adjacent_posts_rel_link_wp_head', 10, 0);
    remove_action('wp_head', 'feed_links_extra', 3);
    remove_action('wp_head', 'wp_generator');
    remove_action('wp_head', 'print_emoji_detection_script', 7);
    remove_action('admin_print_scripts', 'print_emoji_detection_script');
    remove_action('wp_print_styles', 'print_emoji_styles');
    remove_action('admin_print_styles', 'print_emoji_styles');

    function add_styles_and_scripts(){
    global $template;

        wp_enqueue_style( "reset-sheet", get_template_directory_uri() . "/css/reset.css");
        wp_enqueue_style( "style-sheet", get_template_directory_uri() . "/style.css");
        wp_enqueue_script("mail-go-script", get_template_directory_uri() . "/assets/js/mailgo.min.js", true);
      
    }

    add_action( 'wp_enqueue_scripts', 'add_styles_and_scripts' );

    function custom_post_type_noticias() {
        register_post_type('noticia', array(
            'label' => 'Notícias',
            'description' => 'Notícias',
            'menu_position' =>  2,
            'public' => true,
            'show_ui' => true,
            'show_in_menu' => true,
            'capability_type' => 'post',
            'map_meta_cap' => true,
            'hierarchical' => false,
            'query_var' => true,
            'supports' => array('title', 'editor'),
     
            'labels' => array (
                'name' => 'Notícia',
                'singular_name' => 'Notícia',
                'menu_name' => 'Notícias',
                'add_new' => 'Nova Notícia',
                'add_new_item' => 'Adicionar Nova Notícia',
                'edit' => 'Editar Notícia',
                'edit_item' => 'Editar Notícia',
                'new_item' => 'Nova Notícia',
                'view' => 'Ver Notícia',
                'view_item' => 'Ver Notícia',
                'search_items' => 'Procurar Notícia',
                'not_found' => 'Nenhuma Notícia Encontrada',
                'not_found_in_trash' => 'Nenhuma Notícia Encontrada no Lixo',
            )
        ));
    }
    add_action('init', 'custom_post_type_noticias');



?>