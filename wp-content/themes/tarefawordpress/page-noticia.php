<?php 
    // Template Name: Noticia

    $args = array(
        'post_type' => 'noticia'
    );
    $the_query = new WP_Query($args)
    ?>
    <?php get_header();?>


<main id="noticias">
        <section id="sct-2">
            <div class="container">
                <h1>Últimas Notícias</h1>
                <div class="last-news">
                <?php if ( $the_query->have_posts()) : 
                    while( $the_query->have_posts()) : 
                        $the_query->the_post(); ?>
                        <div class="card">
                                <h5><?php the_title() ?></h5>
                                <p><?php the_content() ?></p>
                                <a href="<?php the_permalink() ?>"> Continuar Lendo </a>
                        </div>
                    <?php endwhile;
                else: ?>
                    <p><?php _e('Sem post com seus criterios'); ?> </p>
                <?php endif; ?>
                    <!-- <div class="card">
                        <h5>Título da Notícia</h5>
                        <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Ullam quisquam nisi culpa, debitis repellendus dolore placeat officiis fugit inventore, nemo repudiandae odio, voluptatem ad officia? Excepturi reprehenderit eaque magni beatae.</p> 
                        <a href="#">Continuar Lendo</a> 
                    </div>
                    <div class="card">
                        <h5>Título da Notícia</h5>
                        <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Ullam quisquam nisi culpa, debitis repellendus dolore placeat officiis fugit inventore, nemo repudiandae odio, voluptatem ad officia? Excepturi reprehenderit eaque magni beatae.</p> 
                        <a href="#">Continuar Lendo</a> 
                    </div>
                    <div class="card">
                        <h5>Título da Notícia</h5>
                        <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Ullam quisquam nisi culpa, debitis repellendus dolore placeat officiis fugit inventore, nemo repudiandae odio, voluptatem ad officia? Excepturi reprehenderit eaque magni beatae.</p> 
                        <a href="#">Continuar Lendo</a> 
                    </div>
                    <div class="card">
                        <h5>Título da Notícia</h5>
                        <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Ullam quisquam nisi culpa, debitis repellendus dolore placeat officiis fugit inventore, nemo repudiandae odio, voluptatem ad officia? Excepturi reprehenderit eaque magni beatae.</p> 
                        <a href="#">Continuar Lendo</a> 
                    </div>
                    <div class="card">
                        <h5>Título da Notícia</h5>
                        <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Ullam quisquam nisi culpa, debitis repellendus dolore placeat officiis fugit inventore, nemo repudiandae odio, voluptatem ad officia? Excepturi reprehenderit eaque magni beatae.</p> 
                        <a href="#">Continuar Lendo</a> 
                    </div>
                    <div class="card">
                        <h5>Título da Notícia</h5>
                        <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Ullam quisquam nisi culpa, debitis repellendus dolore placeat officiis fugit inventore, nemo repudiandae odio, voluptatem ad officia? Excepturi reprehenderit eaque magni beatae.</p> 
                        <a href="#">Continuar Lendo</a> 
                    </div> -->
                </div>
                <a href="/todas-noticias.html">Ver Todas</a>
                <p style="display: none;">Não há notícias no momento!</p>
                <div class="card links-uteis">
                    <h4>Links Úteis</h4>
                    <ul>
                        <li><a href="#">Link para site</a></li>
                        <li><a href="#">Link para site</a></li>
                        <li><a href="#">Link para site</a></li>
                        <li><a href="#">Link para site</a></li>
                    </ul>
                </div>
            </div>
        </section>
    </main>

<?php get_footer(); ?>